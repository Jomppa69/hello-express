const express = require("express");
const app = express();
const PORT = 3000;

// GET endpoint - http://120.0.0.1:3000/
app.get("/", (req, res) => {
    res.send("Welcome world!");
});

/**
 * This function multiplies multiplicant with multiplier.
 * @param {number} multiplicant first param
 * @param {number} multiplier second param
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    const product = multiplicant + multiplier;
    return product;
};

// GET multiply endpoint - http://127.0.0.1:3000/multiply?a=1&b=5
app.get("/multiply", (req, res) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplier) || isNaN(multiplicant)) throw new Error("Invalid value.");
        const product = multiply(multiplicant, multiplier);
        console.log({ multiplicant, multiplier, product });
        res.send(product.toString(10));
    } catch(err){
        console.log(err);
        res.send("DOES NOT COMPUTE! Try again with a number ;).")
    }
});

app.listen(PORT, () => console.log(
    `Listening at http://127.0.0.1:${PORT}`
    ));