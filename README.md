# hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the depedencies.

    npm i

Start the REST API service:

`node app.js`

Or int the latest NodeJS version (>=20)
`node --watch app.js`